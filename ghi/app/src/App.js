import Nav from './Nav';
import React from 'react';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendeeForm from './AttendeeForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';
import { BrowserRouter, Routes, Route } from "react-router-dom";

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="my-5 container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="/locations">
            <Route path="/locations/new" element={<LocationForm />} />
          </Route>
          <Route path="/attendees">
            <Route path="" element={<AttendeesList attendees={props.attendees} />} />
            <Route path="/attendees/new" element={<AttendeeForm />} />
          </Route>
          <Route path="/conferences">
            <Route path="/conferences/new" element={<ConferenceForm />} />
          </Route>
          <Route path="/presentations">
            <Route path="/presentations/new" element={<PresentationForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
