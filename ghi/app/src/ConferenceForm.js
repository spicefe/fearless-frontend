import React, { useEffect, useState, } from 'react';

function ConferenceForm () {
    const [locations, setLocations] = useState([]);
    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const [startDate, setStartDate] = useState('');
    const handleStartDateChange = (event) => {
        const value = event.target.value;
        setStartDate(value);
    }
    const [endDate, setEndDate] = useState('');
    const handleEndDateChange = (event) => {
        const value = event.target.value;
        setEndDate(value);
    }
    const [description, setDescription] = useState('');
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }
    const [maximumPresentations, setMaximumPresentations] = useState('');
    const handleMaximumPresentationChange = (event) => {
        const value = event.target.value;
        setMaximumPresentations(value);
    }
    const [maximumAttendees, setMaximumAttendees] = useState('');
    const handleMaximumAttendeesChange = (event) => {
        const value = event.target.value;
        setMaximumAttendees(value);
    }
    const [location, setLocation] = useState('');
    const handleLocationChange = (event) => {
        const selectedIndex = event.target.selectedIndex;
        const locationId = event.target.options[selectedIndex].value;
        setLocation(locationId);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.starts = startDate;
        data.ends = endDate;
        data.description = description;
        data.max_presentations = maximumPresentations;
        data.max_attendees = maximumAttendees;
        data.location = location;
        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            setName('');
            setStartDate('');
            setEndDate('');
            setDescription('');
            setMaximumPresentations('');
            setMaximumAttendees('');
            setLocation('');
        }
    }
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
      }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={startDate} onChange={handleStartDateChange} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control"/>
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input value={endDate} onChange={handleEndDateChange} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control"/>
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description" className="form-label">Description</label>
                <textarea value={description} onChange={handleDescriptionChange} required type="text" name="description" id="description" className="form-control"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input value={maximumPresentations} onChange={handleMaximumPresentationChange} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                <label htmlFor="max_presentations">Maximum presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input value={maximumAttendees} onChange={handleMaximumAttendeesChange} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                <label htmlFor="max_attendees">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select value={location} onChange={handleLocationChange} required id="location" name="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                    return (
                        <option key={location.id} value={location.id}>
                            {location.name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default ConferenceForm;
