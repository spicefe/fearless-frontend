// Get the cookie out of the cookie store
const payloadCookie = await cookieStore.get("jwt_access_payload");
if (payloadCookie) {
  // The cookie value is a JSON-formatted string, so parse it
//   const encodedPayload = JSON.parse(payloadCookie.value);

  // Convert the encoded payload from base64 to normal string
  const decodedPayload = atob(payloadCookie.value);

  // The payload is a JSON-formatted string, so parse it
  const payload = JSON.parse(decodedPayload);

  // Print the payload
  console.log(payload)

  // Check if "events.add_conference" is in the permissions.
  // If it is, remove 'd-none' from the link
  const conferenceAdder = payload.user.perms.includes("events.add_conference")
  if (conferenceAdder) {
    const conferenceNav = document.getElementById("new-conference-nav");
    conferenceNav.classList.remove('d-none');
  }

  // Check if "events.add_location" is in the permissions.
  // If it is, remove 'd-none' from the link
  const locationAdder = payload.user.perms.includes("events.add_location")
  if (locationAdder) {
    const locationNav = document.getElementById("new-location-nav");
    locationNav.classList.remove('d-none');
  }

  const presentationAdder = payload.user.perms.includes("presentations.add_presentation")
  if (presentationAdder) {
    const presentationNav = document.getElementById("new-presentation-nav");
    presentationNav.classList.remove('d-none');
  }
}
