// Create a new JavaScript file to handle loading the available locations and submitting the conference data.
// The value of the options for the locations select tag should be the value of the "id" property of the location object. (This is comparable to what you did on the last page with setting the value of the option to the "abbreviation" property.)
// Notice that the JSON expects a numeric identifier for the location. You'll need to change the location list API to include the id property of locations.
// Make sure you POST to the correct API URL.
// Clear the data from the form when it succeeds.

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/';
    try {
        const response = await fetch(url);
        if (!response.ok) {
            throw new Error('Response not ok');
        } else {
            const data = await response.json();

            const selectTag = document.getElementById('location');

            for (let location of data.locations) {
                const option = document.createElement('option');
                option.value = location.id;
                option.innerHTML = location.name;
                selectTag.appendChild(option);
            }

            const formTag = document.getElementById('create-conference-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData));

                const conferenceUrl = 'http://localhost:8000/api/conferences/';
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                const response = await fetch(conferenceUrl, fetchConfig);
                if (response.ok) {
                    formTag.reset();
                    const newConference = await response.json();
                }
            });

        }
    } catch (e) {
        console.error('error', e);
    }
});
