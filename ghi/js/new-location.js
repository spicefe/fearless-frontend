// "We need to add an event listener for when the DOM loads."
// "Let's declare a variable that will hold the URL for the API that we just created."
// "Let's fetch the URL. Don't forget the await keyword so that we get the response, not the Promise."
// "If the response is okay, then let's get the data using the .json method. Don't forget to await that, too."

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/states/';
    try {
        const response = await fetch(url);
        if (!response.ok) {
            throw new Error('Response not ok');
        } else {
            const data = await response.json();

            // Get the select tag element by its id 'state'
            const selectTag = document.getElementById('state');

            // For each state in the states property of the data

                // Create an 'option' element

                // Set the '.value' property of the option element to the
                // state's abbreviation

                // Set the '.innerHTML' property of the option element to
                // the state's name

                // Append the option element as a child of the select tag
            for (let state of data.states) {
                const option = document.createElement('option');
                option.value = state.abbreviation;
                option.innerHTML = state.name;
                selectTag.appendChild(option);
            }

            const formTag = document.getElementById('create-location-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData));

                const locationUrl = 'http://localhost:8000/api/locations/';
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                const response = await fetch(locationUrl, fetchConfig);
                if (response.ok) {
                    formTag.reset();
                    const newLocation = await response.json();
                }
            });

        }
    } catch (e) {
        console.error('error', e);
    }
});
